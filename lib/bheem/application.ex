defmodule Bheem.Application do
  @moduledoc false
  alias Amqpx.Helper
  use Application

  def start(_type, _args) do
    children = [
      BheemWeb.Telemetry,
      {Phoenix.PubSub, name: Bheem.PubSub},
      BheemWeb.Endpoint,
      Bheem.Scheduler,
      Bheem.Repo,
      Bheem.Broadway.Sample
    ]

    amqp = [
      Helper.manager_supervisor_configuration(Application.get_env(:bheem, :amqp_connection)),
      Helper.producer_supervisor_configuration(Application.get_env(:bheem, :producer))
    ]

    opts = [strategy: :one_for_one, name: Bheem.Supervisor]
    Supervisor.start_link(children ++ amqp, opts)
  end

  def config_change(changed, _new, removed) do
    BheemWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
