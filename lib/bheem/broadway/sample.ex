defmodule Bheem.Broadway.Sample do
  use Broadway
  alias Broadway.Message

  def start_link(_opts) do
    Broadway.start_link(__MODULE__,
      name: __MODULE__,
      producer: [
        module:
          {BroadwayRabbitMQ.Producer,
           queue: "bheem",
           connection:
             Application.get_env(
               :bheem,
               :amqp_connection
             )}
      ],
      processors: [
        default: []
      ]
    )
  end

  def handle_message(_, %Message{data: data} = message, _) do
    case Jason.decode(data) do
      {:ok, data} ->
        resp =
          %{
            filed: data["sensorType"],
            value: data["reading"],
            timestamp: data["timestamp"] |> Timex.from_unix()
          }
          |> Bheem.Model.Sample.insert()

        case resp do
          {:ok, _} ->
            Message.ack_immediately(message)

          {:error, _} ->
            Message.failed(message, "failed")
        end

      {:error, _} ->
        Message.failed(message, "failed")
    end
  end

  def handle_batch(_, messages, _, _) do
    messages
  end
end
