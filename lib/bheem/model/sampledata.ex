defmodule Bheem.Model.Sample do
  use Ecto.Schema
  import Ecto.Query, warn: false
  import Ecto.Changeset
  alias Bheem.Repo

  @cast [:filed, :value, :timestamp]
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "time" do
    field :filed, :string
    field :value, :float
    field :timestamp, :utc_datetime
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, @cast)
    |> validate_required(@cast)
  end

  def insert(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
    |> Repo.insert()
  end

  def query(date) do
    start = Timex.parse!(date, "{YYYY}-{0M}-{D}")
    end_date = Timex.shift(start, days: 1)
    query =
      from u in Bheem.Model.Sample,
      where: u.timestamp > ^start,
      where: u.timestamp < ^end_date

    Repo.all(query)
  end
end
