defmodule Bheem.Sample do
  @doc """
  {reading: 26.0, timestamp: 1511161234, sensorType: “Temperature”}
  """
  def simulate_data() do
    data = %{
      reading: Enum.random(0..30),
      timestamp: Timex.now() |> Timex.to_unix(),
      sensorType: "Temperature"
    }

    Jason.encode!(data)
  end

  alias Amqpx.Gen.Producer

  def send_payload(payload) do
    Producer.publish("bheem", "bheem", payload)
  end

  def cron() do
    simulate_data()
    |> send_payload
  end
end
