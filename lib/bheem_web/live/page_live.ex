defmodule BheemWeb.PageLive do
  use BheemWeb, :live_view
  use Phoenix.HTML
  alias BheemWeb.LayoutView
  alias Contex.{Sparkline}

  # def render(assigns) do
  # LayoutView.render("page_live.html", assigns)
  # end

  def render(assigns) do
    ~L"""
    <div class="py-20">
      <h3>Bheem Example</h3>
      <div class="container">
        <div class="row">
          <div class="column">
            <form phx-change="chart_options_changed">
              <label for="refresh_rate">Refresh Rate</label>
              <input type="number" name="refresh_rate" id="refresh_rate" placeholder="Enter refresh rate" value=<%= @chart_options.refresh_rate %>>
              <label for="refresh_rate">Date</label>
              <input type="date" name="date" id="date" value=<%= @chart_options.date %>>
            </form>
            <%= make_plot(@test_data) %>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(chart_options: %{refresh_rate: 60000, number_of_points: 50, date: Timex.today()})
      |> assign(process_counts: [0])
      |> fetch_data(Timex.today()|> Date.to_string())

    if connected?(socket),
      do: Process.send_after(self(), :tick, socket.assigns.chart_options.refresh_rate)

    {:ok, socket}
  end

  def handle_event("chart_options_changed", %{} = params, socket) do
    options =
      socket.assigns.chart_options
      |> update_if_positive_int(:refresh_rate, params["refresh_rate"])
      |> Map.put(:date, params["date"])

    socket = assign(socket, chart_options: options)

    socket = fetch_data(socket, socket.assigns.chart_options.date)
    {:noreply, socket}
  end

  def handle_info(:tick, socket) do
    Process.send_after(self(), :tick, socket.assigns.chart_options.refresh_rate)

    socket =
      socket
      |> fetch_data(socket.assigns.chart_options.date)

    {:noreply, socket}
  end

  defp update_if_positive_int(map, key, possible_value) do
    case Integer.parse(possible_value) do
      {val, ""} ->
        if val > 0, do: Map.put(map, key, val), else: map

      _ ->
        map
    end
  end

  def fetch_data(socket, date) do
    res = Bheem.Model.Sample.query(date)

    result =
      case res do
        [] ->
          [[0, 0]]

        _ ->
          res
          |> Enum.map(fn x ->
            [x.timestamp |> DateTime.to_string, x.value]
          end)
      end

    assign(socket, test_data: result)
  end

  defp make_plot(data) do
    dataset = Contex.Dataset.new(data)
    plot_content = Contex.BarChart.new(dataset)
    plot = Contex.Plot.new(600, 400, plot_content)
    output = Contex.Plot.to_svg(plot)
  end
end
