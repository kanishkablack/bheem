defmodule :"Elixir.Bheem.Repo.Migrations.TimeSeries Table" do
  use Ecto.Migration

  def change do
    create table(:time, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:filed, :string)
      add(:value, :float)
      add(:timestamp, :utc_datetime)
    end
  end
end
